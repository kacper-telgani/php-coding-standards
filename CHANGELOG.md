# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.0](https://git.at.proexe.co/proexe/php/coding-standard/compare/v1.2.4...v2.0.0) (2024-01-08)


### Features

* Bump standards to PHP 8.2 ([0dc7b9e](https://git.at.proexe.co/proexe/php/coding-standard/commit/0dc7b9e06b2cdffc6451a12f23fb2a47d4453e39))

### [1.2.4](https://git.at.proexe.co/proexe/php/coding-standard/compare/v1.2.3...v1.2.4) (2023-05-15)

### [1.2.3](https://git.at.proexe.co/proexe/php/coding-standard/compare/v1.2.2...v1.2.3) (2023-05-15)


### Bug Fixes

* Lock plugins version to avoid including new rules ([97ec2f9](https://git.at.proexe.co/proexe/php/coding-standard/commit/97ec2f93c64093beec726cb4acb9d215eff3fab9))

### [1.2.2](https://gitlab.com/proexe-common/php/coding-standard/compare/v1.2.1...v1.2.2) (2022-08-30)


### Bug Fixes

* Custom ValidVariableName.NotCamelCaps sniff dropped and replaced with Squiz.NamingConventions.ValidVariableName.NotCamelCaps ([c2bbb3a](https://gitlab.com/proexe-common/php/coding-standard/commit/c2bbb3a2d838816c7d54bc2f35aea9d5e9c09890))

### [1.2.1](https://gitlab.com/proexe-common/php/coding-standard/compare/v1.2.0...v1.2.1) (2022-08-29)


### Bug Fixes

* Fix invalid path to ruleset. Add exceptions ([51fe3e7](https://gitlab.com/proexe-common/php/coding-standard/commit/51fe3e76da09df41d17ae67b7f8941e7741f15ce))

## [1.2.0](https://gitlab.com/proexe-common/php/coding-standard/compare/v1.1.0...v1.2.0) (2022-08-29)


### Features

* Update to php7.4 ([6007629](https://gitlab.com/proexe-common/php/coding-standard/commit/6007629d3dfbf6a0140248e670faa54d936e1544))

## [1.1.0](https://gitlab.com/proexe-common/php/coding-standards/compare/v1.0.2...v1.1.0) (2021-07-05)


### Features

* Move source code to src directory instead of Proexe ([ca266c5](https://gitlab.com/proexe-common/php/coding-standards/commit/ca266c5efd71c3085a397c134c5f6a969d44bc07))

## [1.1.0](https://gitlab.com/proexe-common/php/coding-standards/compare/v1.0.2...v1.1.0) (2021-07-05)


### Features

* Move source code to src directory instead of Proexe ([ca266c5](https://gitlab.com/proexe-common/php/coding-standards/commit/ca266c5efd71c3085a397c134c5f6a969d44bc07))

### [1.0.2](https://gitlab.com/proexe-common/php-coding-standard/compare/v1.0.1...v1.0.2) (2020-12-29)


### Bug Fixes

* Disabled php_cs ordered_imports rule as is not case sensitive (in conflict with phpcbf rule which is). ([48af1f3](https://gitlab.com/proexe-common/php-coding-standard/commit/48af1f3f94f14e9b758516ea523dbddc18c8db93))

### 1.0.1 (2020-12-09)


### Bug Fixes

* Moved dependencies to required from required-dev ([32e4476](https://gitlab.com/proexe-common/php-coding-standard/commit/32e4476df932145bc489bf6d2e6e1fbc60f1c087))
