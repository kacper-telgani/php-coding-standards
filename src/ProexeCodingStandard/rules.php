<?php

return [
        '@PSR1'                               => true,
        '@PSR2'                               => true,
        'phpdoc_order'                        => true,
        'phpdoc_separation'                   => true,
        'phpdoc_align'                        => true,
        'phpdoc_add_missing_param_annotation' => ['only_untyped' => false],
        'phpdoc_trim'                         => true,
        'phpdoc_var_without_name'             => true,
        'align_multiline_comment'             => true,
        'no_unused_imports'                   => true,
        'linebreak_after_opening_tag'         => true,
        'blank_line_after_opening_tag'        => true,
        'blank_line_before_statement'         => true,
        'blank_line_after_namespace'          => true,
        'no_trailing_whitespace_in_comment'   => true,
        'no_trailing_whitespace'              => true,
        'no_closing_tag'                      => true,
        'array_syntax'                        => ['syntax' => 'short'],
        'ordered_imports'                     => false, //Disabled as is not case sensitive
        'visibility_required'                 => true,
        'no_extra_consecutive_blank_lines'    => true,
        'no_blank_lines_after_class_opening'  => true,
];
